#traffic pulled from logs > formatted and printed > top 10 into abuse IPDB API

#!/bin/bash

API_KEY="5578bac808fe59a06682423f68b5b00edd98f961f6f9bf0eaf0871f39b7c4274aa5160e1f108b4ad"

# IP Address to check
IP_ADDRESS="89.249.74.173"

# API request URL
URL="https://api.abuseipdb.com/api/v2/check"

# Make API request
response=$(curl -s -X GET "$URL" -H "Key: $API_KEY" -d "ipAddress=$IP_ADDRESS")

total_reports=$(echo "$response" | grep -o '"totalReports":[0-9]*' | awk -F: '{print $2}')
abuse_confidence_score=$(echo "$response" | grep -o '"abuseConfidenceScore":[0-9]*' | awk -F: '{print $2}')
is_public=$(echo "$response" | grep -o '"isPublic":\s*[^,]*' | awk -F: '{print $2}' | sed 's/\"//g')

# Output formatted results
echo "IP Address: $IP_ADDRESS"
echo "Total Reports: $total_reports"
echo "Abuse Confidence Score: $abuse_confidence_score"
echo "Is Public: $is_public"


